package Controller;

import Presentation.*;
import doctor.ActivityDto;
import doctor.DoctorService;
import doctor.IntakeDto;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DoctorController {
    DoctorView doctorView;
    GenericView intakesView;
    ActivitiesView activitiesView;
    private URL wsdlURL = new URL("http://localhost:8080/ws/doctor/?wsdl");
    private QName qname = new QName("http://service.com/", "DoctorServiceImplService");
    private Service service=Service.create(wsdlURL,qname);

    private DoctorService doctorService = service.getPort(DoctorService.class);
    public DoctorController() throws MalformedURLException {
        doctorView=new DoctorView();
        doctorView.addIntakesActionListener(new IntakesListener());
        doctorView.addActivitiesActionListener(new ActivitiesListener());
        doctorView.addActivitiesChartActionListener(new ChartListener());
    }

    class ChartListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            List<ActivityDto> activities = doctorService.getAllActivities();

            List<String> activitiesName = new ArrayList<String>();
            for (ActivityDto aux : activities) {
                if (!activitiesName.contains(aux.getName()))
                    activitiesName.add(aux.getName());
            }
            List<ActivityCounter> activitiesPerDays = new ArrayList<ActivityCounter>();

            for (int i = 0; i < activitiesName.size(); i++)
                for (ActivityDto aux : activities) {
                    if (aux.getName().equals(activitiesName.get(i))) {
                        String[] days = aux.getStartTime().split("-");
                        int ok = 0;
                        for (ActivityCounter auxC : activitiesPerDays) {
                            if (auxC.getDay() == Integer.parseInt(days[2]) && auxC.getActivity().equals(activitiesName.get(i))) {
                                auxC.setCounter(auxC.getCounter() + 1);
                                ok = 1;
                            }
                        }
                        if (ok == 0) {
                            activitiesPerDays.add(new ActivityCounter(Integer.parseInt(days[2]), 1, activitiesName.get(i)));
                        }
                    }
                }

            for (String auxS : activitiesName) {
                ActivitiChart activitiChart = new ActivitiChart(activitiesPerDays, auxS);
            }
        }
    }

    class RecommandListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String idActivity=activitiesView.getIdActivity();
            String behavior=activitiesView.getBehavior();
            String recomandation=activitiesView.getRecomandation();
            String result=idActivity+"-"+behavior+"-"+recomandation;
            doctorService.saveRecomandation(result);
        }
    }

    class ActivitiesListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            List<ActivityDto> activitiesList=doctorService.getAllActivities();
            ActivityJtable activityJtable=new ActivityJtable();
            activitiesView=new ActivitiesView(
                    activityJtable.createActivityJtable(validareActivitati(activitiesList)),"Activity Table"
            );
            activitiesView.addRecommandActionListener(new RecommandListener());
        }
        public List<ActivityDto> validareActivitati(List<ActivityDto> activities){
            List<ActivityDto> validity=new ArrayList<ActivityDto>();
            for(ActivityDto aux:activities){
                int ok=0;
                long difference=validActivity(aux.getStartTime(),aux.getEndTime());
                if(aux.getName().equals("Sleeping")) {
                    if (difference > 600) {
                        ok = 1;
                    }
                }
                if(aux.getName().equals("Leaving")) {
                    if (difference > 120) {
                        ok = 1;
                    }
                }
                if(aux.getName().equals("Toileting")) {
                    if (difference > 5) {
                        ok = 1;
                    }
                }
                if (ok==1) validity.add(aux);
            }
            return validity;
        }
    }

    class IntakesListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String patientName=doctorView.getPatientName();
            List<IntakeDto> patientIntakes=doctorService.getAllPatientIntakes(patientName);
            IntakesJTable intakesJTable=new IntakesJTable();
            GenericView genericView=new GenericView(intakesJTable.createJtable(patientIntakes),"Intakes View");
        }
    }

    public long validActivity(String startTime,String endtime){
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
        DateTime myDate1 = dtf.parseDateTime(startTime);
        DateTime myDate2= dtf.parseDateTime(endtime);
        Date inceput=myDate1.toDate();
        Date sfarsit=myDate2.toDate();
        long result = sfarsit.getTime() - inceput.getTime();
        TimeUnit timeUnit=TimeUnit.MINUTES;
        long difference=timeUnit.convert(result, TimeUnit.MILLISECONDS);
        return difference;
    }
}
