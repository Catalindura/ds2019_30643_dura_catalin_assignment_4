package Controller;

import Presentation.LoginView;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion;
import doctor.DoctorService;
import doctor.UserDto;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginController {
    private LoginView loginView;
    private URL wsdlURL = new URL("http://localhost:8080/ws/doctor/?wsdl");
    private QName qname = new QName("http://service.com/", "DoctorServiceImplService");
    private Service service=Service.create(wsdlURL,qname);
    private DoctorService doctorService = service.getPort(DoctorService.class);

    public LoginController() throws MalformedURLException {

        loginView = new LoginView();
        loginView.setVisible(true);

        loginView.addLoginActionListener(new LoginActionListener());
    }


    class LoginActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            String username = loginView.getUsername();
            String password = loginView.getPassword();
            UserDto userDto=doctorService.getUserByName(username);
            if(userDto.getPassword().equals(password))
                if(userDto.getType().equals("medic")) {
                    loginView.setVisible(false);
                    try {
                        new DoctorController();
                    }catch (MalformedURLException exc){
                        exc.printStackTrace();
                    }
                }
        }
    }
    public static void main(String[] args) throws MalformedURLException{
        LoginController loginController=new LoginController();
    }
}
