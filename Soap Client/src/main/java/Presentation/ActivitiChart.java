package  Presentation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import doctor.ActivityDto;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

public class ActivitiChart extends JFrame{

   public ActivitiChart(List<ActivityCounter> activities,String auxS) {
       DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
       for (ActivityCounter aux : activities) {
           if (auxS.equals(aux.getActivity())) {
               System.out.println(auxS+" "+aux.getDay()+" "+aux.getCounter());
               dataset.addValue(aux.getCounter(),auxS,aux.getDay()+"");
           }
       }
                    JFreeChart lineChart = ChartFactory.createLineChart(
                            auxS,
                            "Day","Number of Activities",
                            dataset,
                            PlotOrientation.VERTICAL,
                            true,true,false);

                    ChartPanel chartPanel = new ChartPanel( lineChart );
                    chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
                    this.add(chartPanel);
                    this.setSize(560,367);
                    this.setVisible(true);
                    this.setLocationRelativeTo(null);
                }

    }
