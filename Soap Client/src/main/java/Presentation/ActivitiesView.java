package Presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ActivitiesView extends JFrame{
    private JTable tabel;
    private JButton recommand=new JButton("Recommand");
    private JTextField idActivity=new JTextField(5);
    private JTextField behavior=new JTextField(8);
    private JTextField recomandation=new JTextField(25);

    public ActivitiesView(){

    }
    public ActivitiesView(JTable t, String title){
        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel p3=new JPanel();
        JLabel l1=new JLabel("Id activity");
        JLabel l2=new JLabel("Behavior");
        JLabel l3=new JLabel("Recomandation");
        p1.add(l1);
        p1.add(idActivity);
        p1.add(l2);
        p1.add(behavior);
        p1.add(l3);
        p1.add(recomandation);
        p1.add(recommand);
        tabel=t;
        JScrollPane scroll=new JScrollPane(tabel);
        p3.add(p1);
        p3.add(scroll);
        this.setTitle(title);
        this.add(p3);
        this.setSize(750,550);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void addRecommandActionListener(ActionListener mal) {
        recommand.addActionListener(mal);
    }

    public String getIdActivity(){
        return idActivity.getText();
    }

    public String getBehavior(){
        return behavior.getText();
    }

    public String getRecomandation(){
        return recomandation.getText();
    }
}