package Presentation;

public class ActivityCounter {
    public int day;
    public int counter;
    public String activity;

    public ActivityCounter(int day, int counter,String activity) {
        this.day = day;
        this.counter = counter;
        this.activity=activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
