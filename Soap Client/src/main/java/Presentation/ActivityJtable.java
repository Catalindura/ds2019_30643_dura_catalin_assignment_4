package Presentation;

import doctor.ActivityDto;

import javax.swing.*;
import java.util.List;

public class ActivityJtable extends JFrame {
    public ActivityJtable(){
    }
    public JTable createActivityJtable(List<ActivityDto> activities){
        int col = 9;
        String[] coloane = new String[col];
        String[][] matrice = new String[activities.size()][col];
        coloane[0]="Id";
        coloane[1]="Name";
        coloane[2]="SDay";
        coloane[3]="EDay";
        coloane[4] ="SHour";
        coloane[5]="EHour";
        coloane[6]="SMinute";
        coloane[7]="EMinute";
        coloane[8]="Patient";
        for(int i=0;i<activities.size();i++){
            String[] s=activities.get(i).getStartTime().split("-");
            String[] e=activities.get(i).getEndTime().split("-");
            matrice[i][0]=activities.get(i).getIdActivity()+"";
            matrice[i][1]=activities.get(i).getName();
            matrice[i][2]=s[2];
            matrice[i][3]=e[2];
            matrice[i][4]=s[3];
            matrice[i][5]=e[3];
            matrice[i][6]=s[4];
            matrice[i][7]=e[4];
            matrice[i][8]=activities.get(i).getPatientName();
        }
        JTable tabela=new JTable(matrice,coloane);
        tabela.setSize(700,500);
        return tabela;
    }
}
