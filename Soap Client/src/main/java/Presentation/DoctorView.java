package Presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class DoctorView extends JFrame {
    private JLabel patient=new JLabel("Patient's name");
    private JTextField patientName=new JTextField(10);
    private JButton seeIntakes=new JButton("Patient Intakes");
    private JButton seeActivities=new JButton("See Activities  ");
    private JButton seeChart=new JButton("See Activities Chart");

    public DoctorView(){
        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel l1=new JLabel("                                                   ");
        JLabel l2=new JLabel("                                                                             ");
        JLabel l3=new JLabel("                                                                             ");
        JPanel p2 = new JPanel() ;
        p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
        JPanel p3 = new JPanel() ;
        p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
        p1.add(patient);
        p1.add(patientName);
        p1.add(l1);
        p1.add(seeIntakes);
        p1.add(l2);
        p1.add(seeActivities);
        p1.add(l3);
        p1.add(seeChart);
        p3.add(p1);
        this.add(p3);
        this.setTitle("DoctorView");
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public String getPatientName(){
        return patientName.getText();
    }

    public void addIntakesActionListener(ActionListener mal) {
        seeIntakes.addActionListener(mal);
    }

    public void addActivitiesActionListener(ActionListener mal) {
        seeActivities.addActionListener(mal);
    }

    public void addActivitiesChartActionListener(ActionListener mal) {
        seeChart.addActionListener(mal);
    }

    public static void main(String[] args){
        new DoctorView();
    }
}