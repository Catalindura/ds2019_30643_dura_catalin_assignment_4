package Presentation;
import javax.swing.*;
import java.awt.event.ActionListener;

public class GenericView extends JFrame{
    private JTable tabel;
    private JButton exit=new JButton("Exit");

    public GenericView(){

    }
    public GenericView(JTable t, String title){
        tabel=t;
        JScrollPane scroll=new JScrollPane(tabel);
        this.setTitle(title);
        this.add(scroll);
        this.setSize(550,550);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}