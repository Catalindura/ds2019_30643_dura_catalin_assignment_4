package Presentation;

import doctor.IntakeDto;

import javax.swing.*;
import java.util.List;

public class IntakesJTable extends JFrame {

    public IntakesJTable(){

    }
    public JTable createJtable(List<IntakeDto> intakes){
        int col = 7;
        String[] coloane = new String[col];
        String[][] matrice = new String[intakes.size()][col];
        coloane[0]="Id";
        coloane[1] = "Name Patient";
        coloane[2]="Name Medication";
        coloane[3]="Dosage";
        coloane[4] = "Start time";
        coloane[5]="End time";
        coloane[6]="Taken";
        for(int i=0;i<intakes.size();i++){
            matrice[i][0]=""+intakes.get(i).getIdIntake();
            matrice[i][1]=intakes.get(i).getPatientName();
            matrice[i][2]=intakes.get(i).getMedicationName();
            matrice[i][3]=intakes.get(i).getMedicationDosage();
            matrice[i][4]=intakes.get(i).getStartTime();
            matrice[i][5]=intakes.get(i).getEndTime();
            matrice[i][6]=intakes.get(i).getTaken();
        }

        JTable tabela=new JTable(matrice,coloane);
        return tabela;
    }
}
