package Presentation;


import javax.swing.*;
import java.awt.event.ActionListener;

public class LoginView extends JFrame {
    private JButton login = new JButton("login");
    private JLabel username = new JLabel("Username   ");
    private JLabel password = new JLabel("Password    ");
    private JTextField t1 = new JTextField(10);
    private JTextField t2 = new JTextField(10);

    public LoginView(){
        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        p3. setLayout (new BoxLayout (p3, BoxLayout . Y_AXIS ));
        p1.add(username);
        p1.add(t1);
        p1.add(password);
        p1.add(t2);
        p1.add(login);
        p3.add(p1);
        this.setTitle("LoginView");
        this.add(p3);
        this.setSize(250, 250);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public String getUsername(){
       return t1.getText();
    }
    public String getPassword(){
        return t2.getText();
    }

    public void addLoginActionListener(ActionListener mal) {
        login.addActionListener(mal);
    }
}