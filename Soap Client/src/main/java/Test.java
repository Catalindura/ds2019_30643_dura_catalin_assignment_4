import javax.xml.namespace.QName;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.ws.Service;
import doctor.*;


public class Test {
    public static void main(String[] args) throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8080/ws/doctor/?wsdl");
        QName qname = new QName("http://service.com/", "DoctorServiceImplService");
        Service service=Service.create(wsdlURL,qname);
        System.out.println(qname.toString());
        DoctorService doctorService=service.getPort(DoctorService.class);
        System.out.println(doctorService.getUserByName("catalin").getType());
        System.out.println(doctorService.getAllActivities().get(0).getName());
        System.out.println(doctorService.getAllPatientIntakes("222").get(0).getTaken());
        System.out.println(doctorService.getAllPatientIntakes("222").get(1).getTaken());
    }
}
