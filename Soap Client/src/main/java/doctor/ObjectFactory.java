
package doctor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the doctor package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CevaResponse_QNAME = new QName("http://service.com/", "cevaResponse");
    private final static QName _Ceva_QNAME = new QName("http://service.com/", "ceva");
    private final static QName _GetAllActivitiesResponse_QNAME = new QName("http://service.com/", "getAllActivitiesResponse");
    private final static QName _GetUserByName_QNAME = new QName("http://service.com/", "getUserByName");
    private final static QName _IntakeDto_QNAME = new QName("http://service.com/", "intakeDto");
    private final static QName _SaveRecomandation_QNAME = new QName("http://service.com/", "saveRecomandation");
    private final static QName _GetAllPatientIntakesResponse_QNAME = new QName("http://service.com/", "getAllPatientIntakesResponse");
    private final static QName _GetAllActivities_QNAME = new QName("http://service.com/", "getAllActivities");
    private final static QName _GetAllPatientIntakes_QNAME = new QName("http://service.com/", "getAllPatientIntakes");
    private final static QName _UserDto_QNAME = new QName("http://service.com/", "userDto");
    private final static QName _ActivityDto_QNAME = new QName("http://service.com/", "activityDto");
    private final static QName _GetUserByNameResponse_QNAME = new QName("http://service.com/", "getUserByNameResponse");
    private final static QName _SaveRecomandationResponse_QNAME = new QName("http://service.com/", "saveRecomandationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: doctor
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivityDto }
     * 
     */
    public ActivityDto createActivityDto() {
        return new ActivityDto();
    }

    /**
     * Create an instance of {@link GetUserByNameResponse }
     * 
     */
    public GetUserByNameResponse createGetUserByNameResponse() {
        return new GetUserByNameResponse();
    }

    /**
     * Create an instance of {@link GetAllActivities }
     * 
     */
    public GetAllActivities createGetAllActivities() {
        return new GetAllActivities();
    }

    /**
     * Create an instance of {@link IntakeDto }
     * 
     */
    public IntakeDto createIntakeDto() {
        return new IntakeDto();
    }

    /**
     * Create an instance of {@link Ceva }
     * 
     */
    public Ceva createCeva() {
        return new Ceva();
    }

    /**
     * Create an instance of {@link GetUserByName }
     * 
     */
    public GetUserByName createGetUserByName() {
        return new GetUserByName();
    }

    /**
     * Create an instance of {@link SaveRecomandation }
     * 
     */
    public SaveRecomandation createSaveRecomandation() {
        return new SaveRecomandation();
    }

    /**
     * Create an instance of {@link CevaResponse }
     * 
     */
    public CevaResponse createCevaResponse() {
        return new CevaResponse();
    }

    /**
     * Create an instance of {@link GetAllActivitiesResponse }
     * 
     */
    public GetAllActivitiesResponse createGetAllActivitiesResponse() {
        return new GetAllActivitiesResponse();
    }

    /**
     * Create an instance of {@link GetAllPatientIntakes }
     * 
     */
    public GetAllPatientIntakes createGetAllPatientIntakes() {
        return new GetAllPatientIntakes();
    }

    /**
     * Create an instance of {@link GetAllPatientIntakesResponse }
     * 
     */
    public GetAllPatientIntakesResponse createGetAllPatientIntakesResponse() {
        return new GetAllPatientIntakesResponse();
    }

    /**
     * Create an instance of {@link UserDto }
     * 
     */
    public UserDto createUserDto() {
        return new UserDto();
    }

    /**
     * Create an instance of {@link SaveRecomandationResponse }
     * 
     */
    public SaveRecomandationResponse createSaveRecomandationResponse() {
        return new SaveRecomandationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CevaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "cevaResponse")
    public JAXBElement<CevaResponse> createCevaResponse(CevaResponse value) {
        return new JAXBElement<CevaResponse>(_CevaResponse_QNAME, CevaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ceva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "ceva")
    public JAXBElement<Ceva> createCeva(Ceva value) {
        return new JAXBElement<Ceva>(_Ceva_QNAME, Ceva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllActivitiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getAllActivitiesResponse")
    public JAXBElement<GetAllActivitiesResponse> createGetAllActivitiesResponse(GetAllActivitiesResponse value) {
        return new JAXBElement<GetAllActivitiesResponse>(_GetAllActivitiesResponse_QNAME, GetAllActivitiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getUserByName")
    public JAXBElement<GetUserByName> createGetUserByName(GetUserByName value) {
        return new JAXBElement<GetUserByName>(_GetUserByName_QNAME, GetUserByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntakeDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "intakeDto")
    public JAXBElement<IntakeDto> createIntakeDto(IntakeDto value) {
        return new JAXBElement<IntakeDto>(_IntakeDto_QNAME, IntakeDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveRecomandation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "saveRecomandation")
    public JAXBElement<SaveRecomandation> createSaveRecomandation(SaveRecomandation value) {
        return new JAXBElement<SaveRecomandation>(_SaveRecomandation_QNAME, SaveRecomandation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPatientIntakesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getAllPatientIntakesResponse")
    public JAXBElement<GetAllPatientIntakesResponse> createGetAllPatientIntakesResponse(GetAllPatientIntakesResponse value) {
        return new JAXBElement<GetAllPatientIntakesResponse>(_GetAllPatientIntakesResponse_QNAME, GetAllPatientIntakesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllActivities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getAllActivities")
    public JAXBElement<GetAllActivities> createGetAllActivities(GetAllActivities value) {
        return new JAXBElement<GetAllActivities>(_GetAllActivities_QNAME, GetAllActivities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPatientIntakes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getAllPatientIntakes")
    public JAXBElement<GetAllPatientIntakes> createGetAllPatientIntakes(GetAllPatientIntakes value) {
        return new JAXBElement<GetAllPatientIntakes>(_GetAllPatientIntakes_QNAME, GetAllPatientIntakes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "userDto")
    public JAXBElement<UserDto> createUserDto(UserDto value) {
        return new JAXBElement<UserDto>(_UserDto_QNAME, UserDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "activityDto")
    public JAXBElement<ActivityDto> createActivityDto(ActivityDto value) {
        return new JAXBElement<ActivityDto>(_ActivityDto_QNAME, ActivityDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "getUserByNameResponse")
    public JAXBElement<GetUserByNameResponse> createGetUserByNameResponse(GetUserByNameResponse value) {
        return new JAXBElement<GetUserByNameResponse>(_GetUserByNameResponse_QNAME, GetUserByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveRecomandationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com/", name = "saveRecomandationResponse")
    public JAXBElement<SaveRecomandationResponse> createSaveRecomandationResponse(SaveRecomandationResponse value) {
        return new JAXBElement<SaveRecomandationResponse>(_SaveRecomandationResponse_QNAME, SaveRecomandationResponse.class, null, value);
    }

}
