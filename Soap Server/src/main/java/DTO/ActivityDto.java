package DTO;

import Entity.Activity;
import Entity.Patient;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class ActivityDto {
    private Integer idActivity;
    private String patientName;
    private String patientGender;
    private String name;
    private String startTime;
    private String endTime;
    private String behavior;

    public ActivityDto(){
    }

    public ActivityDto(Integer idActivity, String patientName, String patientGender, String name, String startTime, String endTime,String behavior) {
        this.idActivity = idActivity;
        this.patientName = patientName;
        this.patientGender = patientGender;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.behavior=behavior;
    }

    public ActivityDto(Activity activity){
        this.idActivity=activity.getIdActivity();
        this.patientName=activity.getPatient().getName();
        this.patientGender=activity.getPatient().getGender();
        this.name=activity.getName();
        this.startTime=activity.getStartTime();
        this.endTime=activity.getEndTime();
        this.behavior=activity.getBehavior();
    }
    @XmlAttribute
    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    @XmlElement
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    @XmlElement
    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @XmlElement
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @XmlElement
    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior;
    }
}
