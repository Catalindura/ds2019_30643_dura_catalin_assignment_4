package DTO;

import Entity.Intake;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IntakeDto {
    private Integer idIntake;
    private Integer patientId;
    private String patientName;
    private Integer medicationId;
    private String medicationName;
    private String medicationDosage;
    private String startTime;
    private String endTime;
    private String taken;

    public IntakeDto(){

    }

    public IntakeDto(Integer idIntake, Integer patientId, String patientName,
                     Integer medicationId, String medicationName, String medicationDosage,
                     String startTime, String endTime, String taken) {
        this.idIntake = idIntake;
        this.patientId = patientId;
        this.patientName = patientName;
        this.medicationId = medicationId;
        this.medicationName = medicationName;
        this.medicationDosage = medicationDosage;
        this.startTime = startTime;
        this.endTime = endTime;
        this.taken = taken;
    }

    public IntakeDto(Intake intake){
        this.idIntake = intake.getIdIntake();
        this.patientId = intake.getPatient().getIdPatient();
        this.patientName = intake.getPatient().getName();
        this.medicationId = intake.getMedication().getIdMedication();
        this.medicationName = intake.getMedication().getName();
        this.medicationDosage =intake.getMedication().getDosage();
        this.startTime = intake.getStartTime();
        this.endTime = intake.getEndTime();
        this.taken = intake.getTaken();
    }

    @XmlAttribute
    public Integer getIdIntake() {
        return idIntake;
    }

    public void setIdIntake(Integer idIntake) {
        this.idIntake = idIntake;
    }

    @XmlElement
    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    @XmlElement
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    @XmlElement
    public Integer getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Integer medicationId) {
        this.medicationId = medicationId;
    }

    @XmlElement
    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    @XmlElement
    public String getMedicationDosage() {
        return medicationDosage;
    }

    public void setMedicationDosage(String medicationDosage) {
        this.medicationDosage = medicationDosage;
    }

    @XmlElement
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @XmlElement
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @XmlElement
    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }
}
