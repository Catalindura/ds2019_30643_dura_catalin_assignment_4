package DTO;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
@XmlRootElement
public class RecomandationDto {
    public Integer idRecomandation;
    public int caregiver;
    public int patient;
    public int activity;
    public String recomandation;

    public RecomandationDto(){

    }

    public RecomandationDto(Integer idRecomandation, int caregiver, int patient, int activity, String recomandation) {
        this.idRecomandation = idRecomandation;
        this.caregiver = caregiver;
        this.patient = patient;
        this.activity = activity;
        this.recomandation = recomandation;
    }

    @XmlAttribute
    public Integer getIdRecomandation() {
        return idRecomandation;
    }

    public void setIdRecomandation(Integer idRecomandation) {
        this.idRecomandation = idRecomandation;
    }

    @XmlElement
    public int getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(int caregiver) {
        this.caregiver = caregiver;
    }

    @XmlElement
    public int getPatient() {
        return patient;
    }

    public void setPatient(int patient) {
        this.patient = patient;
    }

    @XmlElement
    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    @XmlElement
    public String getRecomandation() {
        return recomandation;
    }

    public void setRecomandation(String recomandation) {
        this.recomandation = recomandation;
    }
}
