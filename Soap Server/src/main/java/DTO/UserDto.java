package DTO;


import Entity.User;

import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserDto {
    private Integer idUser;
    private String username;
    private String password;
    private String type;

    public UserDto(){

    }

    public UserDto(Integer idUser, String username, String password, String type) {
        this.idUser = idUser;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public UserDto(User user){
        this.idUser=user.getIdUser();
        this.username=user.getUsername();
        this.password=user.getPassword();
        this.type=user.getType();
    }
    @XmlAttribute
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @XmlElement
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlElement
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
