package Entity;


import javax.naming.ldap.StartTlsRequest;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="activity")
public class Activity {
    @Id
    @Column(name="idactivity",nullable=false,unique=true)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idActivity;

    @ManyToOne
    @JoinColumn(name="idpatient")
    private Patient patient;

    @Column(name="name")
    private String name;

    @Column(name="starttime")
    private String startTime;

    @Column(name="endtime")
    private String endTime;

    @Column(name="behavior")
    private String behavior;


    @OneToMany(mappedBy = "activity",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Recomandation> recomandations;

    public Activity(){

    }

    public Activity(Patient patient, String name, String startTime, String endTime,String behavior) {
        this.patient = patient;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.behavior=behavior;
    }

    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior;
    }
}
