package Entity;

import javax.persistence.*;

@Entity
@Table(name="recomandation")
public class Recomandation {
    @Id
    @Column(name="idrecomandation",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idRecomandation;


    @ManyToOne
    @JoinColumn(name="idpatient")
    private Patient patient;


    @ManyToOne
    @JoinColumn(name="idcaregiver")
    private Caregiver caregiver;

    @ManyToOne
    @JoinColumn(name="idactivity")
    private Activity activity;

    @Column(name="description")
    private String description;

    public Recomandation(){

    }

    public Recomandation(Patient patient, Caregiver caregiver, Activity activity, String description) {
        this.patient = patient;
        this.caregiver = caregiver;
        this.activity = activity;
        this.description = description;
    }

    public Integer getIdRecomandation() {
        return idRecomandation;
    }

    public void setIdRecomandation(Integer idRecomandation) {
        this.idRecomandation = idRecomandation;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
