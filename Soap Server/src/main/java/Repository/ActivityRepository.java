package Repository;

import DTO.ActivityDto;
import Entity.Activity;
import Entity.Patient;

import java.util.List;

public interface ActivityRepository {
    public List<ActivityDto> getAllActivities(Patient patient);
    public Activity getActivityById(int id);
    public void updateActivity(Activity activity);
}
