package Repository;

import Entity.Caregiver;

public interface CaregiverRepository {
    public Caregiver getCaregiverById(int id);
}
