package Repository;

import DTO.IntakeDto;
import Entity.Intake;
import Entity.Patient;

import java.util.List;

public interface IntakeRepository {
    public List<IntakeDto> getAllPatientIntakes(Patient patient);
}
