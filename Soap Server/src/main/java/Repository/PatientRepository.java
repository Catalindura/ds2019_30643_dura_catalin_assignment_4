package Repository;

import Entity.Patient;

public interface PatientRepository {
    public Patient getPatientById(int id);
    public Patient getPatientByName(String name);
}
