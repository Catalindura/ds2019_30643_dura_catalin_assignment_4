package Repository;

import Entity.User;

public interface UserRepository {
    public User getUserByName(String name);
}
