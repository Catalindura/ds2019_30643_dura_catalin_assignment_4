package RepositoryImpl;

import DTO.ActivityDto;
import Entity.Activity;
import Entity.Intake;
import Entity.Patient;
import Hybernate.Hybernate;
import Repository.ActivityRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

public class ActivityImpl implements ActivityRepository {
    private SessionFactory sessionFactory= Hybernate.getSessionFactory();

    public List<ActivityDto> getAllActivities(Patient patient) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Activity.class);
        criteria.add(Restrictions.like("patient",patient));
        List<Activity> result=criteria.list();
        session.close();
        List<ActivityDto> resultDto=new ArrayList<ActivityDto>();
        for(Activity aux:result){
            resultDto.add(new ActivityDto(aux));
        }
        return resultDto;
    }

    public Activity getActivityById(int id) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Activity.class);
        criteria.add(Restrictions.like("idActivity",id));
        Activity result=(Activity)criteria.list().get(0);
        session.close();
        return result;
    }

    public void updateActivity(Activity activity) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.merge(activity);
        session.getTransaction().commit();
        session.close();
    }
}
