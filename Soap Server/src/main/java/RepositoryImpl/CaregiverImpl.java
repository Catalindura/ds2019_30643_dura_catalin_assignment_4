package RepositoryImpl;

import Entity.Caregiver;
import Entity.Patient;
import Hybernate.Hybernate;
import Repository.CaregiverRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class CaregiverImpl implements CaregiverRepository {

    private SessionFactory sessionFactory= Hybernate.getSessionFactory();


    public Caregiver getCaregiverById(int id) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Caregiver.class);
        criteria.add(Restrictions.like("idCaregiver",id));
        Caregiver result=(Caregiver)criteria.list().get(0);
        session.close();
        return result;
    }
}
