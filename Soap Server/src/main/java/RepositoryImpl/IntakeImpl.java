package RepositoryImpl;

import DTO.ActivityDto;
import DTO.IntakeDto;
import Entity.Activity;
import Entity.Intake;
import Entity.Patient;
import Entity.User;
import Hybernate.Hybernate;
import Repository.IntakeRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

public class IntakeImpl implements IntakeRepository {
    private SessionFactory sessionFactory= Hybernate.getSessionFactory();

    public List<IntakeDto> getAllPatientIntakes(Patient patient) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Intake.class);
        criteria.add(Restrictions.like("patient",patient));
        List<Intake> result=criteria.list();
        session.close();
        List<IntakeDto> resultDto=new ArrayList<IntakeDto>();
        for(Intake aux:result){
            resultDto.add(new IntakeDto(aux));
        }
        return resultDto;
    }
}
