package RepositoryImpl;

import Entity.Patient;
import Entity.User;
import Hybernate.Hybernate;
import Repository.PatientRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class PatientImpl implements PatientRepository {


    private SessionFactory sessionFactory= Hybernate.getSessionFactory();
    public Patient getPatientById(int id) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Patient.class);
        criteria.add(Restrictions.like("idPatient",id));
        Patient result=(Patient)criteria.list().get(0);
        session.close();
        return result;
    }

    public Patient getPatientByName(String name) {
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Patient.class);
        criteria.add(Restrictions.like("name",name));
        Patient result=(Patient)criteria.list().get(0);
        session.close();
        return result;
    }
}
