package RepositoryImpl;

import Entity.Recomandation;
import Repository.RecomandationRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import Hybernate.Hybernate;

public class RecomandationImpl implements RecomandationRepository {

    private SessionFactory sessionFactory= Hybernate.getSessionFactory();
    public void saveRecomandation(Recomandation recomandation) {
        Session session=sessionFactory.openSession();
        session.save(recomandation);
        session.close();
    }
}
