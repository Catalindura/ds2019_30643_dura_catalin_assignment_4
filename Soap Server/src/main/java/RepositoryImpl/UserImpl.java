package RepositoryImpl;

import Entity.Doctor;
import Entity.Intake;
import Entity.User;
import Repository.UserRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import Hybernate.Hybernate;
import org.hibernate.criterion.Restrictions;

public class UserImpl implements UserRepository {

    private SessionFactory sessionFactory= Hybernate.getSessionFactory();

    public User getUserByName(String name) {
            Session session=sessionFactory.openSession();
            Criteria criteria=session.createCriteria(User.class);
            criteria.add(Restrictions.like("username",name));
            User result=(User)criteria.list().get(0);
            session.close();
            return result;
    }
}
