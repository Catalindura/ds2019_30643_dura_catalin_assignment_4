package com.service;

import DTO.ActivityDto;
import DTO.IntakeDto;
import DTO.RecomandationDto;
import DTO.UserDto;
import Entity.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface DoctorService {

    @WebMethod
    public void saveRecomandation(String recomandation);

    @WebMethod
    public UserDto getUserByName(String name);

    @WebMethod
    public List<ActivityDto> getAllActivities();

    @WebMethod
    public List<IntakeDto> getAllPatientIntakes(String name);
    @WebMethod
    public String ceva();
}