package com.service;

import DTO.ActivityDto;
import DTO.IntakeDto;
import DTO.RecomandationDto;
import DTO.UserDto;
import Entity.*;
import RepositoryImpl.*;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "com.service.DoctorService")
public class DoctorServiceImpl implements DoctorService {

    public DoctorServiceImpl(){

    }

    public ActivityImpl activityImpl=new ActivityImpl();
    public IntakeImpl intakeImpl=new IntakeImpl();
    public UserImpl userImpl=new UserImpl();
    public PatientImpl patientImpl=new PatientImpl();
    public CaregiverImpl caregiverImpl=new CaregiverImpl();
    public RecomandationImpl recomandationImpl=new RecomandationImpl();

    public UserDto getUserByName(String name) {

        User user=userImpl.getUserByName(name);
        return new UserDto(user);
    }

    public List<ActivityDto> getAllActivities() {
        return activityImpl.getAllActivities(patientImpl.getPatientById(31));
    }

    public List<IntakeDto> getAllPatientIntakes(String name) {
        return intakeImpl.getAllPatientIntakes(patientImpl.getPatientByName(name));
    }


    public void saveRecomandation(String recomandation){
        String[] elemente=recomandation.split("-");
        Patient patient=patientImpl.getPatientById(31);
        Activity activity=activityImpl.getActivityById(Integer.parseInt(elemente[0]));
        activity.setBehavior(elemente[1]);
        Caregiver caregiver=patient.getCaregiver();
        if(!activity.getBehavior().equals("normal"))
        recomandationImpl.saveRecomandation(new Recomandation(patient,caregiver,activity,elemente[2]));
        activityImpl.updateActivity(activity);
    }

    public String ceva(){
        return "merge";
    }

    public static void main(String[] args){
        DoctorServiceImpl doctorService=new DoctorServiceImpl();
        System.out.println(doctorService.getAllActivities().get(0).getName());
    }

}

